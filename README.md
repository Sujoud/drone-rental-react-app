# Drone Rental React App

React app enables registered users from renting a drone from thier charging stations.

    
## Technical Details
 
- This app uses localStorage as to simulate backend
- Redux is used to manage the state alongside with localStorage
- Bootstrap is used as css library

## Install 

    $ git clone https://gitlab.com/Sujoud/drone-rental-react-app
    $ cd drone-rental-react-app
    $ yarn install

## Run Application 

    $ yarn start

open http://localhost:3000 in your browser

## Run the test

    $ yarn test